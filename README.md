# 泊客里民宿精选网站（bokerin.com）

#### 介绍
毕业设计

#### 安装教程

1.  idea打开该项目
2.  mysql载入brokerin.sql
3.  检查数据库登入密码
4.  开始编译

#### 说明

1.  部分界面借鉴爱彼迎，纯手写代码，无抄袭行为。
2.  部分界面无法进入（主要是不想改了...）
3.  图片未显示，请从走一遍出租流程，图片资源位置：..\src\main\resources\static\roomPictures
