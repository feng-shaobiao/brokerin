package com.inn.brokerin.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
public class WebConfigurer implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        InterceptorRegistration interceptorRegistration = registry.addInterceptor(new LoginInterceptor());
        interceptorRegistration.addPathPatterns("/roomList")
                .addPathPatterns("/messageList")
                .addPathPatterns("/deleteMessage")
                .addPathPatterns("/orderList")
                .addPathPatterns("/personal")
                .addPathPatterns("/reviews")
                .addPathPatterns("/editHeadPortrait")
                .addPathPatterns("/contactHost/{id}")
                .addPathPatterns("/rentalHousing")
                .addPathPatterns("/checkIn/{id}");
    }

}