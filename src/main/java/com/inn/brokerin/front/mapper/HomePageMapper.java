package com.inn.brokerin.front.mapper;

import com.inn.brokerin.front.domain.Evaluate;
import com.inn.brokerin.front.domain.OrderForm;
import com.inn.brokerin.front.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface HomePageMapper {
    @Select("SELECT * FROM user")
    List<User> SelectALLuser();

    @Select("SELECT * FROM user WHERE phone=#{phone}")
    User SelectUserByphone(String phone);

    @Update("UPDATE user SET name=#{name},password=#{password},image=#{image},phone=#{phone},status=#{status},email=#{email},time=#{time},introduction=#{introduction},estimate=#{estimate},identity=#{identity},address=#{address} " +
            "WHERE userId=#{userId}")
    int Update(User user);

    @Insert("INSERT INTO user(userId,name,password,image,phone,status,email,time,introduction,estimate,identity,address)"+
            "values (#{userId},#{name},#{password},#{image},#{phone},#{status},#{email},#{time},#{introduction},#{estimate},#{identity},#{address})")
    int NewUser(User user);

    @Insert("INSERT INTO evaluate(evaluateId,contact,evaluatePhone,evaluateName,evaluatePicture,roomId,dataTime,roomName,roomUserPhone,roomPicture,address)"+
            "values (#{evaluateId},#{contact},#{evaluatePhone},#{evaluateName},#{evaluatePicture},#{roomId},#{dataTime},#{roomName},#{roomUserPhone},#{roomPicture},#{address})")
    int NewEvaluate(Evaluate evaluate);

    @Select("SELECT * FROM evaluate WHERE roomId=#{roomId}")
    List<Evaluate> SelectEvaluateByRoomId(int roodId);

    @Select("SELECT * FROM evaluate WHERE evaluatePhone=#{evaluatePhone}")
    List<Evaluate> SelectEvaluateByPhone(String evaluatePhone);

    @Select("SELECT * FROM evaluate WHERE roomUserPhone=#{roomUserPhone}")
    List<Evaluate> SelectEvaluateByUserPhone(String roomUserPhone);
}
