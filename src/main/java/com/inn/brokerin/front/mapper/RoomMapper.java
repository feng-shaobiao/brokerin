package com.inn.brokerin.front.mapper;

import com.inn.brokerin.front.domain.Room;
import com.inn.brokerin.front.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface RoomMapper {
    @Insert("INSERT INTO room(roomId,houseName,landlordPicture,landlordName,landlordPhone,bedroom,bedLabel,yesOrno,bathroomLabel,peopleLabel,searchInMap,detailedAddress,housingTypes,leaseType,bedroomPicture,installation,textMessageForCircum,textMessageForHouse,priceInput,firstPicture,unitPrice,cleaningFee,serviceCharge,status)"+
            "values (#{roomId},#{houseName},#{landlordPicture},#{landlordName},#{landlordPhone},#{bedroom},#{bedLabel},#{yesOrno},#{bathroomLabel},#{peopleLabel},#{searchInMap},#{detailedAddress},#{housingTypes},#{leaseType},#{bedroomPicture},#{installation},#{textMessageForCircum},#{textMessageForHouse},#{priceInput},#{firstPicture},#{unitPrice},#{cleaningFee},#{serviceCharge},#{status})")
    int NewRoom(Room room);

    @Select("SELECT * FROM room")
    List<Room> SelectAllRoom();

    @Select("SELECT * FROM room WHERE searchInMap LIKE CONCAT('%',#{searchInMap},'%')")
    List<Room> SelectRoomByCity(String searchInMap);

    @Select("SELECT * FROM room WHERE roomId=#{roomId}")
    Room SelectRoomById(int roomId);

    @Select("SELECT * FROM room WHERE landlordPhone=#{landlordPhone}")
    List<Room> SelectRoomByPhone(String landlordPhone);
}
