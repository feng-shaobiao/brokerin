package com.inn.brokerin.front.mapper;

import com.inn.brokerin.front.domain.Message;
import com.inn.brokerin.front.domain.OrderForm;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MessageMapper {
    @Select("SELECT * FROM message WHERE messageRePhone=#{messageRePhone}")
    List<Message> SelectMessageByPhone(String messageRePhone);

    @Delete("DELETE FROM message WHERE messageRePhone=#{messageRePhone}")
    int delete(String messageRePhone);

    @Insert("INSERT INTO message(messageId,messageReceiver,messageRePhone,messageSend,messageSePhone,content,dateTime)"+
            "values (#{messageId},#{messageReceiver},#{messageRePhone},#{messageSend},#{messageSePhone},#{content},#{dateTime})")
    int NewMessage(Message message);
}
