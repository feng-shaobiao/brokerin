package com.inn.brokerin.front.mapper;

import com.inn.brokerin.front.domain.OrderForm;
import com.inn.brokerin.front.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface OrderMapper {
    @Insert("INSERT INTO orderform(orderId,checkInTime,dayNumber,peopleNumber,tenantName,tenantIdentity,unitPrice,cleaningFee,serviceCharge,priceInput,searchInMap,textMessageForHouse,phone,picture,roomID)"+
            "values (#{orderId},#{checkInTime},#{dayNumber},#{peopleNumber},#{tenantName},#{tenantIdentity},#{unitPrice},#{cleaningFee},#{serviceCharge},#{priceInput},#{searchInMap},#{textMessageForHouse},#{phone},#{picture},#{roomId})")
    int NewOrder(OrderForm orderform);

    @Select("SELECT * FROM orderform WHERE phone=#{phone}")
    List<OrderForm> SelectAllOrderByPhone(String phone);
}
