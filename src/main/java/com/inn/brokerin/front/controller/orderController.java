package com.inn.brokerin.front.controller;

import com.inn.brokerin.front.domain.OrderForm;
import com.inn.brokerin.front.domain.User;
import com.inn.brokerin.front.mapper.HomePageMapper;
import com.inn.brokerin.front.mapper.OrderMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

//订单控制器
@Controller
public class orderController {
    //通用代码all
    void allUse(Model model, HttpSession session){
        if (session.getAttribute("phone")==null)
        {
            User user=new User();
            user.setStatus(1);
            model.addAttribute("user1",user);
        }
        else {
            Object object=session.getAttribute("phone");
            String phone = String.valueOf(object);
            User user=homePageMapper.SelectUserByphone(phone);
            model.addAttribute("user1",user);
        }
    }
    @Resource
    HomePageMapper homePageMapper;

    @Resource
    OrderMapper orderMapper;


    @GetMapping("/orderList")
    public String orderList(Model model,HttpSession session){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        List<OrderForm>orderForms=orderMapper.SelectAllOrderByPhone(phone);
        model.addAttribute("orderForms",orderForms);
        return "mainPages/front/orderList";
    }
}
