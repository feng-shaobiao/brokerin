package com.inn.brokerin.front.controller;

import com.inn.brokerin.front.domain.Message;
import com.inn.brokerin.front.domain.Room;
import com.inn.brokerin.front.domain.User;
import com.inn.brokerin.front.mapper.HomePageMapper;
import com.inn.brokerin.front.mapper.MessageMapper;
import com.inn.brokerin.front.mapper.RoomMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class messageController {

    @Resource
    HomePageMapper homePageMapper;

    @Resource
    MessageMapper messageMapper;

    @Resource
    RoomMapper roomMapper;


    //通用代码all
    void allUse(Model model,HttpSession session){
        if (session.getAttribute("phone")==null)
        {
            User user=new User();
            user.setStatus(1);
            model.addAttribute("user1",user);
        }
        else {
            Object object=session.getAttribute("phone");
            String phone = String.valueOf(object);
            User user=homePageMapper.SelectUserByphone(phone);
            model.addAttribute("user1",user);
        }
    }

    @GetMapping("/messageList")
    public String messageList(Model model,HttpSession session){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        List<Message> messages=messageMapper.SelectMessageByPhone(phone);
        model.addAttribute("messages",messages);
        return "mainPages/front/messageList";
    }

    @GetMapping("/deleteMessage")
    public String deleteMessage(Model model,HttpSession session){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        messageMapper.delete(phone);
        List<Message> messages=messageMapper.SelectMessageByPhone(phone);
        model.addAttribute("messages",messages);
        return "mainPages/front/messageList";
    }


    @PostMapping("/sendMessage/{id}")
    public String sendMessage(Model model,HttpSession session,@PathVariable int id,Message message) {
        allUse(model, session);
        Room room=roomMapper.SelectRoomById(id);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user=homePageMapper.SelectUserByphone(phone);
        message.setMessageReceiver(room.getLandlordName());
        message.setMessageSend(user.getName());
        message.setMessageSePhone(user.getPhone());
        message.setMessageRePhone(room.getLandlordPhone());
        Date time = new Date();
        DateFormat createFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String createTime = createFormat.format(time);
        message.setDateTime(createTime);
        messageMapper.NewMessage(message);
        model.addAttribute("room",room);
        return "mainPages/front/contactHost";
    }

    @PostMapping("/messageListSendMessage/{id}")
    public String messageListSendMessage(Model model,HttpSession session,@PathVariable String id,Message message) {
        allUse(model, session);
        User user1=homePageMapper.SelectUserByphone(id);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user=homePageMapper.SelectUserByphone(phone);
        message.setMessageReceiver(user1.getName());
        message.setMessageSend(user.getName());
        message.setMessageSePhone(user.getPhone());
        message.setMessageRePhone(user1.getPhone());
        Date time = new Date();
        DateFormat createFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String createTime = createFormat.format(time);
        message.setDateTime(createTime);
        messageMapper.NewMessage(message);
        List<Message> messages=messageMapper.SelectMessageByPhone(phone);
        model.addAttribute("messages",messages);
        return "mainPages/front/messageList";
    }
}
