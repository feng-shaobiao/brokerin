package com.inn.brokerin.front.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.inn.brokerin.front.domain.Evaluate;
import com.inn.brokerin.front.domain.User;
import com.inn.brokerin.front.mapper.HomePageMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.List;

//个人控制器-所有涉及个人的页面控制都在这：编辑、保存等
@Controller
public class personalController {
    //通用代码all
    void allUse(Model model, HttpSession session){
        if (session.getAttribute("phone")==null)
        {
            User user=new User();
            user.setStatus(1);
            model.addAttribute("user1",user);
        }
        else {
            Object object=session.getAttribute("phone");
            String phone = String.valueOf(object);
            User user=homePageMapper.SelectUserByphone(phone);
            model.addAttribute("user1",user);
        }
    }

    @Resource
    HomePageMapper homePageMapper;

    @GetMapping("/personal")
    public String personal(Model model,HttpSession session){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        List<Evaluate>evaluates=homePageMapper.SelectEvaluateByPhone(phone);
        model.addAttribute("totalNum",evaluates.size());
        return "mainPages/front/personal";
    }

    @GetMapping("/reviews")
    public String reviews(Model model,HttpSession session){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        List<Evaluate>evaluates=homePageMapper.SelectEvaluateByPhone(phone);
        model.addAttribute("evaluates",evaluates);
        model.addAttribute("num",evaluates.size());
        return "mainPages/front/reviews";
    }

    @GetMapping("/editHeadPortrait")
    public String editHeadPortrait(Model model,HttpSession session){
        allUse(model,session);
        return "mainPages/front/editHeadPortrait";
    }

    @PostMapping("/editInfo")
    public String editInfo(Model model,HttpSession session,User user){
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user1=homePageMapper.SelectUserByphone(phone);
        user1.setAddress(user.getAddress());
        user1.setIntroduction(user.getIntroduction());
        user1.setPassword(user.getPassword());
        user1.setEmail(user.getEmail());
        homePageMapper.Update(user1);
        allUse(model,session);
        return "mainPages/front/personal";
    }

    @ResponseBody
    @RequestMapping("/uploadFile")
    public JSON uploadFile(MultipartFile file, HttpServletRequest request,HttpSession session) {
        JSONObject json=new JSONObject();
        String path = System.getProperty("user.dir");
        String filePath = path+"/src/main/resources/static/images/";
        File file1 = new File(filePath);
        if (!file1.exists()) {
            file1.mkdirs();
        }
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user=homePageMapper.SelectUserByphone(phone);
        String fileName=file.getOriginalFilename();
        user.setImage("/images/"+fileName);
        homePageMapper.Update(user);
        String finalFilePath = filePath + file.getOriginalFilename().trim();
        File desFile = new File(finalFilePath);
        if (desFile.exists()) {
            desFile.delete();
        }
        try {
            file.transferTo(desFile);
            json.put("code",0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            json.put("code",1);
        }
        return json;
    }
}
