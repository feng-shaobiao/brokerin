package com.inn.brokerin.front.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.inn.brokerin.front.domain.Room;
import com.inn.brokerin.front.domain.User;
import com.inn.brokerin.front.mapper.HomePageMapper;
import com.inn.brokerin.front.mapper.RoomMapper;
import com.inn.brokerin.service.SendEmailService;
import com.mysql.cj.Session;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//前台控制器：包含登录、注册、帮助、出租房源等功能
@Controller
public class homePageController {
    //通用代码all
    void allUse(Model model,HttpSession session){
        if (session.getAttribute("phone")==null)
        {
            User user=new User();
            user.setStatus(1);
            model.addAttribute("user1",user);
        }
        else {
            Object object=session.getAttribute("phone");
            String phone = String.valueOf(object);
            User user=homePageMapper.SelectUserByphone(phone);
            model.addAttribute("user1",user);
        }
    }
    @Resource
    HomePageMapper homePageMapper;

    @Resource
    RoomMapper roomMapper;

    @GetMapping("/brokerin")
    public String homePage(HttpSession session,Model model){
        allUse(model,session);
        List<Room>rooms=roomMapper.SelectRoomByCity("南宁");
        List<Room>rooms1=roomMapper.SelectRoomByCity("上海");
        List<Room>rooms2=roomMapper.SelectRoomByCity("北京");
        List<Room>rooms3=roomMapper.SelectRoomByCity("湛江");
        List<Room>rooms4=roomMapper.SelectRoomByCity("广州");
        List<Room>rooms5=roomMapper.SelectRoomByCity("深圳");
        List<Room>rooms6=roomMapper.SelectRoomByCity("重庆");
        model.addAttribute("rooms",rooms);
        model.addAttribute("rooms1",rooms1);
        model.addAttribute("rooms2",rooms2);
        model.addAttribute("rooms3",rooms3);
        model.addAttribute("rooms4",rooms4);
        model.addAttribute("rooms5",rooms5);
        model.addAttribute("rooms6",rooms6);
        return "mainPages/front/homePage";
    }

    @GetMapping("/host")
    public String host(HttpSession session,Model model){
        allUse(model,session);
        return "mainPages/front/host";
    }

    @GetMapping("/forgotPassword")
    public String forgotPassword(HttpSession session,Model model){
        allUse(model,session);
        return "mainPages/front/forgotPassword";
    }

    @GetMapping("/help")
    public String help(HttpSession session,Model model){
        allUse(model,session);
        return "mainPages/front/help";
    }

    @PostMapping("/login")
    public String Login(Model model, User user, HttpSession session){
        List<User> userList=homePageMapper.SelectALLuser();
        for (User user1 : userList) {
            if (user1.getPhone().equals(user.getPhone()) && user1.getPassword().equals(user.getPassword())) {
                user1.setStatus(0);
                homePageMapper.Update(user1);
                session.setAttribute("phone", user.getPhone());
                List<Room>rooms=roomMapper.SelectRoomByCity("南宁");
                List<Room>rooms1=roomMapper.SelectRoomByCity("上海");
                List<Room>rooms2=roomMapper.SelectRoomByCity("北京");
                List<Room>rooms3=roomMapper.SelectRoomByCity("湛江");
                List<Room>rooms4=roomMapper.SelectRoomByCity("广州");
                List<Room>rooms5=roomMapper.SelectRoomByCity("深圳");
                List<Room>rooms6=roomMapper.SelectRoomByCity("重庆");
                model.addAttribute("rooms",rooms);
                model.addAttribute("rooms1",rooms1);
                model.addAttribute("rooms2",rooms2);
                model.addAttribute("rooms3",rooms3);
                model.addAttribute("rooms4",rooms4);
                model.addAttribute("rooms5",rooms5);
                model.addAttribute("rooms6",rooms6);
                model.addAttribute("user1",user1);
                return "mainPages/front/homePage";
            }
        }
        return "mainPages/front/homePage";
    }

    @GetMapping("/loginout")
    public String Loginout(HttpSession session,Model model){
        session.setAttribute("phone",null);
        allUse(model,session);
        return "mainPages/front/homePage";
    }

    @GetMapping("/searchResult")
    public String searchResult(HttpSession session,Model model){
        allUse(model,session);
        return "mainPages/front/searchResult";
    }

    @PostMapping("/register")
    public String register(HttpSession session,Model model,User user){
        allUse(model,session);
        user.setStatus(0);
        user.setImage("/images/personal1.jpg");
        user.setName("请改名.");
        Date time = new Date();
        DateFormat createFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String createTime = createFormat.format(time);
        user.setTime(createTime);
        homePageMapper.NewUser(user);
        session.setAttribute("phone", user.getPhone());
        model.addAttribute("user1",user);
        return "mainPages/front/personal";
    }

    @GetMapping("/editPw")
    public String editPw(HttpSession session,Model model,HttpServletRequest request){
        allUse(model,session);
        String phone = request.getParameter("phone");
        User user=homePageMapper.SelectUserByphone(phone);
        if (user==null){
            return "mainPages/front/forgotPassword";
        }
        else {
            user.setPassword("123456");
            homePageMapper.Update(user);
            return "mainPages/front/homePage";
        }
    }

}
