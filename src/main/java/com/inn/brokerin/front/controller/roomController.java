package com.inn.brokerin.front.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.inn.brokerin.front.domain.*;
import com.inn.brokerin.front.mapper.HomePageMapper;
import com.inn.brokerin.front.mapper.MessageMapper;
import com.inn.brokerin.front.mapper.OrderMapper;
import com.inn.brokerin.front.mapper.RoomMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//房源控制器-房源详情、对房子下订单、联系房东、出租房源等
@Controller
public class roomController {
    //通用代码all
    void allUse(Model model,HttpSession session){
        if (session.getAttribute("phone")==null)
        {
            User user=new User();
            user.setStatus(1);
            model.addAttribute("user1",user);
        }
        else {
            Object object=session.getAttribute("phone");
            String phone = String.valueOf(object);
            User user=homePageMapper.SelectUserByphone(phone);
            model.addAttribute("user1",user);
        }
    }
    @Resource
    HomePageMapper homePageMapper;

    @Resource
    RoomMapper roomMapper;

    @Resource
    OrderMapper orderMapper;

    @Resource
    MessageMapper messageMapper;

    @GetMapping("/roomDetails/{id}")
    public String roomDetails(Model model, @PathVariable int id,HttpSession session){
        allUse(model,session);
        Room room=roomMapper.SelectRoomById(id);
        List<Evaluate>evaluates=homePageMapper.SelectEvaluateByRoomId(id);
        String date[]=room.getBedroomPicture().split("/");
        String arr[] = new String[date.length-1];
        for (int i=1;i<date.length;i++)
        {
            arr[i-1]="/roomPictures/"+date[i];
        }
        model.addAttribute("pictures",arr);
        model.addAttribute("evaluates",evaluates);
        model.addAttribute("num",evaluates.size());
        model.addAttribute("room",room);
        return "mainPages/front/roomDetails";
    }

    @PostMapping("/checkIn/{id}")
    public String checkIn(Model model, HttpSession session, @PathVariable int id, OrderForm orderForm){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user=homePageMapper.SelectUserByphone(phone);
        Room room=roomMapper.SelectRoomById(id);
        long day=0;
        String date[]=orderForm.getDayNum().split("-");
        String str1=orderForm.getDayNum().substring(0,orderForm.getDayNum().length() - 13);
        String str2=orderForm.getDayNum().substring(13,orderForm.getDayNum().length());
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date state = sdf.parse(str1);
            Date end  = sdf.parse(str2);
            long stateTimeLong = state.getTime();
            long endTimeLong = end.getTime();
            // 结束时间-开始时间 = 天数
            day = (endTimeLong-stateTimeLong)/(24*60*60*1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        orderForm.setDayNumber(Integer.parseInt(String.valueOf(day)));
        orderForm.setCheckInTime(orderForm.getDayNum());
        orderForm.setCleaningFee(room.getCleaningFee());
        orderForm.setServiceCharge(room.getServiceCharge());
        orderForm.setUnitPrice(room.getUnitPrice());
        orderForm.setPriceInput(room.getPriceInput());
        orderForm.setPicture(room.getFirstPicture());
        orderForm.setSearchInMap(room.getSearchInMap());
        orderForm.setPhone(user.getPhone());
        orderForm.setRoomId(room.getRoomId());
        orderForm.setTextMessageForHouse(room.getTextMessageForHouse());
        orderMapper.NewOrder(orderForm);
        model.addAttribute("room",room);
        model.addAttribute("orderForm",orderForm);
        return "mainPages/front/checkIn";
    }

    @GetMapping("/contactHost/{id}")
    public String contactHost(Model model,@PathVariable int id,HttpSession session){
        allUse(model,session);
        Room room=roomMapper.SelectRoomById(id);
        model.addAttribute("room",room);
        return "mainPages/front/contactHost";
    }

    @GetMapping("/rentalHousing")
    public String rentalHousing(Model model,HttpSession session){
        allUse(model,session);
        return "mainPages/front/rentalHousing";
    }

    @GetMapping("/roomList")
    public String roomList(Model model,HttpSession session){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        List<Room> rooms=roomMapper.SelectRoomByPhone(phone);
        List<Evaluate>evaluates=homePageMapper.SelectEvaluateByUserPhone(phone);
        model.addAttribute("rooms",rooms);
        model.addAttribute("evaluates",evaluates);
        return "mainPages/front/roomList";
    }
    String fileName="";
    @PostMapping("/checkHouse")
    public String checkHouse(Model model, Room room,HttpSession session){
        allUse(model,session);
        room.setBedroomPicture(fileName);
        String date[]=fileName.split("/");
        room.setFirstPicture("/roomPictures/"+date[1]);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user=homePageMapper.SelectUserByphone(phone);
        room.setLandlordName(user.getName());
        room.setLandlordPhone(user.getPhone());
        room.setPriceInput(room.getUnitPrice()+room.getCleaningFee()+room.getServiceCharge());
        room.setStatus("/images/inReview.png");
        room.setLandlordPicture(user.getImage());
        roomMapper.NewRoom(room);
        return "redirect:/brokerin";
    }


    @ResponseBody
    @RequestMapping("/uploadRoomFile")
    public JSON uploadFile(MultipartFile file, HttpServletRequest request, HttpSession session) {
        JSONObject json=new JSONObject();
        String path = System.getProperty("user.dir");
        String filePath = path+"/src/main/resources/static/roomPictures/";
        File file1 = new File(filePath);
        if (!file1.exists()) {
            file1.mkdirs();
        }
        fileName=fileName+"/"+file.getOriginalFilename();
        String finalFilePath = filePath + file.getOriginalFilename().trim();
        File desFile = new File(finalFilePath);
        if (desFile.exists()) {
            desFile.delete();
        }
        try {
            file.transferTo(desFile);
            json.put("code",0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            json.put("code",1);
        }
        return json;
    }

    @GetMapping("/search")
    public String search(Model model,HttpServletRequest request,HttpSession session){
        allUse(model,session);
        String searchInMap = request.getParameter("searchInMap");
        List<Room> rooms=roomMapper.SelectRoomByCity(searchInMap);
        model.addAttribute("num",rooms.size());
        model.addAttribute("rooms",rooms);
        return "mainPages/front/searchResult";
    }


    @GetMapping("/searchCity")
    public String searchCity(Model model,HttpServletRequest request,HttpSession session){
        allUse(model,session);
        String searchInMap = request.getParameter("searchCity");
        List<Room> rooms=roomMapper.SelectRoomByCity(searchInMap);
        model.addAttribute("rooms",rooms);
        return "mainPages/front/roomList";
    }

    @PostMapping("/evaluate/{id}")
    public String evaluate(Model model, HttpSession session,@PathVariable int id, Evaluate evaluate){
        allUse(model,session);
        Object object=session.getAttribute("phone");
        String phone = String.valueOf(object);
        User user=homePageMapper.SelectUserByphone(phone);
        Room room=roomMapper.SelectRoomById(id);
        evaluate.setEvaluateName(user.getName());
        evaluate.setEvaluatePhone(user.getPhone());
        evaluate.setRoomId(id);
        Date time = new Date();
        DateFormat createFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String createTime = createFormat.format(time);
        evaluate.setDataTime(createTime);
        evaluate.setRoomName(room.getHouseName());
        evaluate.setRoomUserPhone(room.getLandlordPhone());
        evaluate.setAddress(room.getDetailedAddress());
        evaluate.setRoomPicture(room.getFirstPicture());
        evaluate.setEvaluatePicture(user.getImage());
        homePageMapper.NewEvaluate(evaluate);
        return "redirect:/orderList";
    }


}
