package com.inn.brokerin.front.domain;

public class OrderForm {
    int orderId;
    String checkInTime;
    int dayNumber;
    String dayNum;
    int peopleNumber;
    String  tenantName;
    String tenantIdentity;
    int unitPrice;
    int cleaningFee;
    int  serviceCharge;
    String message;
    int priceInput;
    int roomId;
    String textMessageForHouse;
    String picture;
    String phone;
    String  searchInMap;

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(int dayNumber) {
        this.dayNumber = dayNumber;
    }

    public int getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantIdentity() {
        return tenantIdentity;
    }

    public void setTenantIdentity(String tenantIdentity) {
        this.tenantIdentity = tenantIdentity;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(int cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public int getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(int serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPriceInput() {
        return priceInput;
    }

    public void setPriceInput(int priceInput) {
        this.priceInput = priceInput;
    }

    public String getDayNum() {
        return dayNum;
    }

    public void setDayNum(String dayNum) {
        this.dayNum = dayNum;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getSearchInMap() {
        return searchInMap;
    }

    public void setSearchInMap(String searchInMap) {
        this.searchInMap = searchInMap;
    }

    public String getTextMessageForHouse() {
        return textMessageForHouse;
    }

    public void setTextMessageForHouse(String textMessageForHouse) {
        this.textMessageForHouse = textMessageForHouse;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
