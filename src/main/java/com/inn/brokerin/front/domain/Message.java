package com.inn.brokerin.front.domain;

public class Message {

    int messageId;
    String messageReceiver;
    String messageRePhone;
    String messageSend;
    String  messageSePhone;
    String content;
    String dateTime;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageReceiver() {
        return messageReceiver;
    }

    public void setMessageReceiver(String messageReceiver) {
        this.messageReceiver = messageReceiver;
    }

    public String getMessageRePhone() {
        return messageRePhone;
    }

    public void setMessageRePhone(String messageRePhone) {
        this.messageRePhone = messageRePhone;
    }

    public String getMessageSend() {
        return messageSend;
    }

    public void setMessageSend(String messageSend) {
        this.messageSend = messageSend;
    }

    public String getMessageSePhone() {
        return messageSePhone;
    }

    public void setMessageSePhone(String messageSePhone) {
        this.messageSePhone = messageSePhone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
