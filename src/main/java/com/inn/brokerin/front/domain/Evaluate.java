package com.inn.brokerin.front.domain;

public class Evaluate {
    int evaluateId;
    int roomId;
    String contact;
    String evaluatePhone;
    String evaluateName;
    String evaluatePicture;
    String dataTime;
    String roomName;
    String roomUserPhone;
    String roomPicture;
    String address;

    public String getRoomPicture() {
        return roomPicture;
    }

    public void setRoomPicture(String roomPicture) {
        this.roomPicture = roomPicture;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRoomUserPhone() {
        return roomUserPhone;
    }

    public void setRoomUserPhone(String roomUserPhone) {
        this.roomUserPhone = roomUserPhone;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }

    public String getEvaluatePicture() {
        return evaluatePicture;
    }

    public void setEvaluatePicture(String evaluatePicture) {
        this.evaluatePicture = evaluatePicture;
    }

    public int getEvaluateId() {
        return evaluateId;
    }

    public void setEvaluateId(int evaluateId) {
        this.evaluateId = evaluateId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEvaluatePhone() {
        return evaluatePhone;
    }

    public void setEvaluatePhone(String evaluatePhone) {
        this.evaluatePhone = evaluatePhone;
    }

    public String getEvaluateName() {
        return evaluateName;
    }

    public void setEvaluateName(String evaluateName) {
        this.evaluateName = evaluateName;
    }
}
