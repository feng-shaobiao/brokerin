package com.inn.brokerin.front.domain;

public class Room {

    int roomId;
    String houseName;
    String landlordName;
    String landlordPhone;
    int bedroom;
    int bedLabel;
    String yesOrno;
    int bathroomLabel;
    int peopleLabel;
    String searchInMap;
    String detailedAddress;
    String housingTypes;
    String leaseType;
    String bedroomPicture;
    String installation;
    String textMessageForCircum;
    String textMessageForHouse;
    int unitPrice;
    int cleaningFee;
    int serviceCharge;
    int priceInput;
    String firstPicture;
    String status;
    String landlordPicture;

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getLandlordPicture() {
        return landlordPicture;
    }

    public void setLandlordPicture(String landlordPicture) {
        this.landlordPicture = landlordPicture;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public int getBedroom() {
        return bedroom;
    }

    public void setBedroom(int bedroom) {
        this.bedroom = bedroom;
    }

    public int getBedLabel() {
        return bedLabel;
    }

    public void setBedLabel(int bedLabel) {
        this.bedLabel = bedLabel;
    }

    public String getYesOrno() {
        return yesOrno;
    }

    public void setYesOrno(String yesOrno) {
        this.yesOrno = yesOrno;
    }

    public int getBathroomLabel() {
        return bathroomLabel;
    }

    public void setBathroomLabel(int bathroomLabel) {
        this.bathroomLabel = bathroomLabel;
    }

    public int getPeopleLabel() {
        return peopleLabel;
    }

    public void setPeopleLabel(int peopleLabel) {
        this.peopleLabel = peopleLabel;
    }

    public String getSearchInMap() {
        return searchInMap;
    }

    public void setSearchInMap(String searchInMap) {
        this.searchInMap = searchInMap;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public String getHousingTypes() {
        return housingTypes;
    }

    public void setHousingTypes(String housingTypes) {
        this.housingTypes = housingTypes;
    }

    public String getLeaseType() {
        return leaseType;
    }

    public void setLeaseType(String leaseType) {
        this.leaseType = leaseType;
    }


    public String getBedroomPicture() {
        return bedroomPicture;
    }

    public void setBedroomPicture(String bedroomPicture) {
        this.bedroomPicture = bedroomPicture;
    }

    public String getInstallation() {
        return installation;
    }

    public void setInstallation(String installation) {
        this.installation = installation;
    }

    public String getTextMessageForCircum() {
        return textMessageForCircum;
    }

    public void setTextMessageForCircum(String textMessageForCircum) {
        this.textMessageForCircum = textMessageForCircum;
    }

    public String getTextMessageForHouse() {
        return textMessageForHouse;
    }

    public void setTextMessageForHouse(String textMessageForHouse) {
        this.textMessageForHouse = textMessageForHouse;
    }

    public int getPriceInput() {
        return priceInput;
    }

    public void setPriceInput(int priceInput) {
        this.priceInput = priceInput;
    }

    public String getFirstPicture() {
        return firstPicture;
    }

    public void setFirstPicture(String firstPicture) {
        this.firstPicture = firstPicture;
    }

    public String getLandlordName() {
        return landlordName;
    }

    public void setLandlordName(String landlordName) {
        this.landlordName = landlordName;
    }

    public String getLandlordPhone() {
        return landlordPhone;
    }

    public void setLandlordPhone(String landlordPhone) {
        this.landlordPhone = landlordPhone;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(int cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public int getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(int serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
