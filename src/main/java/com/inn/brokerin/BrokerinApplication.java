package com.inn.brokerin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class BrokerinApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrokerinApplication.class, args);
    }

}
