package com.inn.brokerin.config;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号,开发时使用沙箱提供的APPID，生产环境改成自己的APPID
    public static String APP_ID = "2021000117641796";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCcJ8jjIO/miAU7RQ2T2cfIKcuusMXtGLmLHAikHbqJRGj3UkjiWw4pOIaKWOyP0vVAMo02REo8dwaA+saqXyy9SyhcliCeeB4TuctyA8YopnGBoHvVN1I3/kT0CWD7nl+9WHkuVQKnZaTMWFb2CSMxMciY67PVxECv3CYkJZDBO1FFMbj/uk+7vOz4Q9OJfuqf+V4vPq4/axp/yFk8GB+M8eNeQZxx/MYEf9VjVLcbrDNV3trWM4Myq81SOy/+59lRyz6VRibdSZW7A/VEx3UsaoQ1Vc2A6Z0Pd+kTrzFM4QifbFRQIBIGi5GctADY2zX8rLaPU/HkvhImYYcp3wazAgMBAAECggEAcLANOksElLpH/DOizrV0hLAUuqX6z1TuYMMEQVrqyDBEB2asDh1jpL+T57awUEIOlhtk6GpzoBrM6LsQLtar9JaOM/Jq67EHE3kapdavY5ppgwALHRzlCl9F5031XN83e+6i1JqoZaDmQkjlyHtjmBazChfeGT+c5svaAm/RfH9rounPqIDS3Ln6WicFD5PIQ7aIBHz9aGhtbJ4HqEWnbptg0p/znixk16K4U0V9aTNaPr71hFdXZ/BtHjxK39z2cOb3mvEXQCkaAhdCN1OaiY2JTb8bCNxtjKFXCz0/70CGdW0ojZzsIlyGcU5vhBR2hx5rqBfzzGSXc3b0AfqA8QKBgQDqx/bLzY8qpNNhdVZG7pCQg3YNikCZ4oG5XqT6ZUFsaUakcgDobA+BFAMSOkzcTOxTyr1eYZpsDOQG52eUutspGKCKPiVMBxq76Vc4+Sf/EaShlXzKU65+QKU2J5xbhEzdjTntruudwJbl2d9OATlldUminZbsn9yg1gmQjix/jwKBgQCqRLCDrJtZESuF4ZqSr6WzQ+/CbhP94XyJaC4/vt+1YP5FzuC9u7nfjgGsDs+1OijxCZ/5egnuHEl2rlRJtqdFDDMBUuRC4KkbV3yfotZZcTIba8MYuGwjRfUNHtLkF7auykACWGHF1CDxy3GKLW1svO1BZkJ2D+3W0TD14Lh0nQKBgQDpaoyO6nsfpmcRSZrRQnUvd8NyirTXST43kUTsPKbipXe6Hi2Q5X40kjTLdnLbww5h7/p9TwQ70EUUKBPu5ibloLpbNpXWFGob0E0LM4r0KBJJI1nBvZiJpLOcN51XzxysV6/0Aif12ncrZYSBBO6PVCA1L//QmM68iI9RzEepOQKBgQCmHzBst8o1Ywgork1GUnMDbQQOmaGNsSL1WRzOeub+quxKpaYl13NpBr7g5O6ILX9mdHKNUCFCHH0cesenx7p5JobZAsvL6C91iqz/fztMtqEhmckWZFC1NXr52xn9Cpnk/VToNNzny6h84HYdT2cUkVuV1DSBxdbSIPV431B4DQKBgBYMInzKXbgqIaGvcC0E/bTZOlS5BxjOM6ngBUVSme8l6U3HwGvj4syWiteyV+WaGruSyMZHxz2C0tUuL+58qJDyA4TL0589BeTMoVRu55UnOdYpQrU7fSsYPyhaQIfC5VCC5O33kJnwBkKtrGqflSQSGeiwJdvBRUpo8MXPpFnU";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnCfI4yDv5ogFO0UNk9nHyCnLrrDF7Ri5ixwIpB26iURo91JI4lsOKTiGiljsj9L1QDKNNkRKPHcGgPrGql8svUsoXJYgnngeE7nLcgPGKKZxgaB71TdSN/5E9Alg+55fvVh5LlUCp2WkzFhW9gkjMTHImOuz1cRAr9wmJCWQwTtRRTG4/7pPu7zs+EPTiX7qn/leLz6uP2saf8hZPBgfjPHjXkGccfzGBH/VY1S3G6wzVd7a1jODMqvNUjsv/ufZUcs+lUYm3UmVuwP1RMd1LGqENVXNgOmdD3fpE68xTOEIn2xUUCASBouRnLQA2Ns1/Ky2j1Px5L4SJmGHKd8GswIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8089/brokerin";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://localhost:8089/brokerin";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "utf-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
