package com.inn.brokerin.rear.mapper;

import com.inn.brokerin.front.domain.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import com.inn.brokerin.rear.domain.Admin;

@Mapper
public interface AdminMapper {
    @Select("SELECT * FROM admin")
    List<Admin> SelectAllAdmin();

    @Select("SELECT * FROM admin WHERE adminId=#{adminId}")
    Admin SelectAdminByAdminName(String adminId);

    @Delete("DELETE FROM user WHERE userId=#{userId}")
    int delete(Long userId);

    @Update("UPDATE room SET status=#{status} WHERE roomId=#{roomId}")
    int Update(@Param("roomId") Long roomId, @Param("status") String status);
}
