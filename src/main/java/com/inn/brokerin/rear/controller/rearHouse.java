package com.inn.brokerin.rear.controller;

import com.inn.brokerin.front.domain.Room;
import com.inn.brokerin.front.mapper.RoomMapper;
import com.inn.brokerin.rear.domain.Admin;
import com.inn.brokerin.rear.mapper.AdminMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class rearHouse {
    //通用代码all
    void adminAllUse(Model model, HttpSession session){
        Object object = session.getAttribute("adminId");
        String adminId = String.valueOf(object);
        Admin admin = adminMapper.SelectAdminByAdminName(adminId);
        model.addAttribute("admin",admin);
    }
    @Resource
    AdminMapper adminMapper;
    @Resource
    RoomMapper roomMapper;

    @GetMapping("/rearHouse")
    public String rearHouse(Model model, HttpSession session){
        adminAllUse(model,session);
        List<Room> rooms=roomMapper.SelectAllRoom();
        model.addAttribute("rooms",rooms);
        return "mainPages/rear/rearHouse";
    }

    @GetMapping("/alreadyPass/{id}")
    public String alreadyPass(Model model, HttpSession session,@PathVariable Long id){
        adminAllUse(model,session);
        adminMapper.Update(id,"/images/alreadyPass.png");
        return "redirect:/rearHouse";
    }

    @GetMapping("/notPass/{id}")
    public String notPass(Model model, HttpSession session,@PathVariable Long id){
        adminAllUse(model,session);
        adminMapper.Update(id,"/images/notPass.png");
        return "redirect:/rearHouse";
    }
}
