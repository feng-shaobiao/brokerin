package com.inn.brokerin.rear.controller;

import com.inn.brokerin.front.domain.User;
import com.inn.brokerin.front.mapper.HomePageMapper;
import com.inn.brokerin.rear.domain.Admin;
import com.inn.brokerin.rear.mapper.AdminMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class rearUser {
    //通用代码all
    void adminAllUse(Model model, HttpSession session){
        Object object = session.getAttribute("adminId");
        String adminId = String.valueOf(object);
        Admin admin = adminMapper.SelectAdminByAdminName(adminId);
        model.addAttribute("admin",admin);
    }
    @Resource
    AdminMapper adminMapper;
    @Resource
    HomePageMapper homePageMapper;

    @GetMapping("/rearUser")
    public String rearUser(Model model, HttpSession session){
        adminAllUse(model,session);
        List<User> userList=homePageMapper.SelectALLuser();
        model.addAttribute("userList",userList);
        return "mainPages/rear/rearUser";
    }

    @GetMapping("/deleteUser/{id}")
    public String deleteUser(Model model, HttpSession session,@PathVariable Long id){
        adminMapper.delete(id);
        adminAllUse(model,session);
        return "redirect:/rearUser";
    }

    @PostMapping("/passRearLogin")
    public String passRearLogin(Admin admin, Model model,HttpSession session){
        List<Admin> adminList = adminMapper.SelectAllAdmin();
        for (Admin admin1 : adminList){
            if (admin1.getAdminId()== admin.getAdminId() && admin1.getAdminPassword().equals(admin.getAdminPassword())){
                session.setAttribute("adminId", admin1.getAdminId());
                return "redirect:/rearUser";
            }
        }
        return "mainPages/rear/rearLogin";
    }
}
