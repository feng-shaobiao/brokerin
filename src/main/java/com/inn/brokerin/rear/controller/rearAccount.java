package com.inn.brokerin.rear.controller;

import com.inn.brokerin.rear.domain.Admin;
import com.inn.brokerin.rear.mapper.AdminMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
public class rearAccount {

    //通用代码all
    void adminAllUse(Model model, HttpSession session){
        Object object = session.getAttribute("adminId");
        String adminId = String.valueOf(object);
        Admin admin = adminMapper.SelectAdminByAdminName(adminId);
        model.addAttribute("admin",admin);
    }
    @Resource
    AdminMapper adminMapper;

    @GetMapping("/accountManage")
    public String accountManage(Model model, HttpSession session){
        adminAllUse(model,session);
        return "mainPages/rear/accountManage";
    }
}
