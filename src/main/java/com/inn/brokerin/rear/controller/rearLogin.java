package com.inn.brokerin.rear.controller;

import com.inn.brokerin.front.domain.User;
import com.inn.brokerin.front.mapper.HomePageMapper;
import com.inn.brokerin.rear.domain.Admin;
import com.inn.brokerin.rear.mapper.AdminMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class rearLogin {
    //通用代码all
    void adminAllUse(Model model,HttpSession session){
        Object object = session.getAttribute("adminId");
        String adminId = String.valueOf(object);
        Admin admin = adminMapper.SelectAdminByAdminName(adminId);
        model.addAttribute("admin",admin);
    }
    @Resource
    AdminMapper adminMapper;

    @Resource
    HomePageMapper homePageMapper;

    @GetMapping("/rearLogin")
    public String rearLogin(){
        return "mainPages/rear/rearLogin";
    }


    @GetMapping("/rearLoginout")
    public String rearLoginout(){
        return "mainPages/rear/rearLogin";
    }
}
